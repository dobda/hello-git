# Hello git

git과 친해져보자

## 커리큘럼

- ### Fork 설치하기

  [Fork 사이트 링크](https://git-fork.com/)

  - `Github Desktop`, `SourceTree` 이런거 두고 Fork를 사용하는 이유?

  1. Github Desktop은 간결함을 추구하는데 커밋 그래프조차 표현이 안 되므로 제외
  1. SourceTree와 Fork는 기본 기능들이 모두 구비되어있고, Git-flow, Submodule, 외부 프로그램 연동과 같은 고급 기능과 편의 기능도 다양하게 제공한다.
  1. 이 중 SourceTree는 소스 규모가 커질수록 속도 저하가 크고, 프로그램 자체가 좀 최적화가 부족해서 느리고 자주 팅김(4년전 사용 경험.. 지금은 나아졌을지도?)
  1. 또한 SourceTree는 Bitbucket(Atlassian)의 서비스를 사용하지 않더라도 기본적으로 계정을 요구한다.
  1. Fork는 모든 기능이 기본 무료로 제공되고, Github, Bigbucket과 같이 다른 서비스에 종속되지 않은 프로그램이기 때문에 좀 더 다양한 자유로운? 사용 경험을 준다.

- ### Git과, Git Remote, Local repo의 차이 구분하기

  많은 사람들이 Git과 Github의 차이를 구분하지 못 한다.\
  Git과 Github의 차이는 무엇이며, remote와 local은 무엇인지 먼저 알아보자.

- ### Git repository란?

  Git repository(저장소)란 무엇인지, .git 폴더는 무엇인지, Git repo를 직접 생성해보고, 클론 해보며 이해해보자.

- ### Git이 하는 일

  간단하게 파일을 수정하고 커밋해보며 Git이 하는 일이 무엇인지, 왜 Git을 써야하는지 이해해보자.

- ### Git의 기본 기능 이해하기

    - Local Changes, Stage, Commit
    - Checkout
    - HEAD에 대한 이해
    - Stash
    - Revert와 Reset
    - tag
    - diff
    - 파일 단위로 History 확인하기
    - 과거의 파일 상태 확인하기

- ### 협업을 도와주는 Git의 기능

    - Branch
    - Push, Fetch, Pull
        - local과 remote의 차이 이해
        - local과 remote의 branch 이해
    - Cherry-pick
    - Merge

- ### 충돌 해결하기
    - 외부 Merge 도구 설정하기(VS Code 사용)
    - 다른 사람의 코드와 충돌을 일으키고 해결해보기

- ### Git-flow 맛보기
